#!/usr/bin/env python

import ao
import mad
import readline
import socket
import struct
import sys
import threading
from time import sleep
from enum import Enum


SERVER_BUFFER = 4096
CLIENT_BUFFER = 32768
HEADER_TYPE = 'i'
HEADER_SIZE = struct.calcsize('i')

class Protocol(Enum):
    LIST = 0
    PLAY = 1
    STOP = 2
    DOWNLOAD = 3
    QUIT = 4
    NULL = -1

# The Mad audio library we're using expects to be given a file object, but
# we're not dealing with files, we're reading audio data over the network.  We
# use this object to trick it.  All it really wants from the file object is the
# read() method, so we create this wrapper with a read() method for it to
# call, and it won't know the difference.
# NOTE: You probably don't need to modify this class.
class mywrapper(object):
    def __init__(self):
        self.mf = None
        self.data = ""

    # When it asks to read a specific size, give it that many bytes, and
    # update our remaining data.
    def read(self, size):
        result = self.data[:size]
        self.data = self.data[size:]
        return result


# Helper functions
def send_data(data):
    sys.stdout.write(data)
    sys.stdout.flush()

def reset_wrap(wrap, cond_filled):
    cond_filled.acquire()
    wrap.data = ""
    wrap.mf = mad.MadFile(wrap)
    cond_filled.notify()
    cond_filled.release()

def socket_flush(s):
    s.setblocking(False)
    try:
        c_recv = s.recv(CLIENT_BUFFER)
        while len(c_recv) >= CLIENT_BUFFER:
            c_recv = s.recv(CLIENT_BUFFER)
    except socket.error:
        pass

    s.setblocking(True)


# Receive messages.  If they're responses to info/list, print
# the results for the user to see.  If they contain song data, the
# data needs to be added to the wrapper object.  Be sure to protect
# the wrapper with synchronization, since the other thread is using
# it too!
def recv_thread_func(wrap, cond_filled, sock):
    while True:
        # TODO
        packet = sock.recv(SERVER_BUFFER)
        res = struct.unpack(HEADER_TYPE, packet[:HEADER_SIZE])[0]
        data = packet[HEADER_SIZE:]

        if res == Protocol.LIST.value:
            send_data(data)

        elif res == Protocol.PLAY.value:
            reset_wrap(wrap, cond_filled)
            send_data(data)

        elif res == Protocol.STOP.value:
            send_data(data)

        elif res == Protocol.DOWNLOAD.value:
            if data != 'EOF':
                cond_filled.acquire()
                wrap.data += data
                cond_filled.notify()
                cond_filled.release()

                pack = struct.pack('ii', Protocol.DOWNLOAD.value, -1)
                sock.send(pack)
            else:
                print("Playing complete!")

        elif res == Protocol.NULL.value:
            send_data(data)

        else:
            continue


# If there is song data stored in the wrapper object, play it!
# Otherwise, wait until there is.  Be sure to protect your accesses
# to the wrapper with synchronization, since the other thread is
# using it too!
def play_thread_func(wrap, cond_filled, dev):
    while True:
        """
        TODO
        example usage of dev and wrap (see mp3-example.py for a full example):
        buf = wrap.mf.read()
        dev.play(buffer(buf), len(buf))
        """
        cond_filled.acquire()
        buf = wrap.mf.read()
        if buf:
            dev.play(buffer(buf), len(buf))
        cond_filled.notify()
        cond_filled.release()


def main():
    if len(sys.argv) < 3:
        print('Usage: %s <server name/ip> <server port>' % sys.argv[0])
        sys.exit(1)

    # Create a pseudo-file wrapper, condition variable, and socket.  These will
    # be passed to the thread we're about to create.
    wrap = mywrapper()
    wrap.mf = mad.MadFile(wrap)

    # Create a condition variable to synchronize the receiver and player threads.
    # In python, this implicitly creates a mutex lock too.
    # See: https://docs.python.org/2/library/threading.html#condition-objects
    cond_filled = threading.Condition()

    # Create a TCP socket and try connecting to the server.
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((sys.argv[1], int(sys.argv[2])))

    # Create a thread whose job is to receive messages from the server.
    recv_thread = threading.Thread(
        target=recv_thread_func,
        args=(wrap, cond_filled, sock)
    )
    recv_thread.daemon = True
    recv_thread.start()

    # Create a thread whose job is to play audio file data.
    dev = ao.AudioDevice('pulse')
    play_thread = threading.Thread(
        target=play_thread_func,
        args=(wrap, cond_filled, dev)
    )
    play_thread.daemon = True
    play_thread.start()

    # Enter our never-ending user I/O loop.  Because we imported the readline
    # module above, raw_input gives us nice shell-like behavior (up-arrow to
    # go backwards, etc.).
    while True:
        line = raw_input('>> ')

        if ' ' in line:
            cmd, args = line.split(' ', 1)
        else:
            cmd = line
            args = None

        # TODO: Send messages to the server when the user types things.
        if cmd in ['l', 'list']:
            print('The user asked for list.')
            pack = struct.pack('ii', Protocol.LIST.value, -1)
            sock.send(pack)

        if cmd in ['p', 'play']:
            if args is None:
                print('Usage: p <MusicID>')
            elif args.isdigit():
                print('The user asked to play:', args)
                socket_flush(sock)
                pack = struct.pack('ii', Protocol.PLAY.value, int(args))
                sock.send(pack)
            else:
                print("Input invalid")

        if cmd in ['s', 'stop']:
            print('The user asked for stop.')
            reset_wrap(wrap, cond_filled)
            socket_flush(sock)
            pack = struct.pack('ii', Protocol.STOP.value, -1)
            sock.send(pack)

        if cmd in ['quit', 'q', 'exit']:
            pack = struct.pack('ii', Protocol.QUIT.value, -1)
            sock.send(pack)
            sys.exit(0)


if __name__ == '__main__':
    main()