#!/usr/bin/env python

import os
import socket
import struct
import sys
from threading import Lock, Thread
from enum import Enum


QUEUE_LENGTH = 10
SEND_BUFFER = 4096
HEADER_TYPE = 'i'
HEADER_SIZE = struct.calcsize('i')
READ_SIZE = SEND_BUFFER - HEADER_SIZE

SONGS = {}
SONGLIST = ""

class Protocol(Enum):
    LIST = 0
    PLAY = 1
    STOP = 2
    DOWNLOAD = 3
    QUIT = 4
    NULL = -1


# per-client struct
class Client:
    def __init__(self):
        self.lock = Lock()

        self.live_session = True
        self.request = None
        self.music_id = None
        self.current_stream = None

    def connect(self, conn):
        self.conn = conn

    def reset(self):
        self.live_session = True
        self.request = None
        self.music_id = None
        if self.current_stream: 
            self.current_stream.close()
        self.current_stream = None


# Helper functions
def pack_protocol(value, data):
    pack = struct.pack(HEADER_TYPE, value) + data
    return pack


# TODO: Thread that sends music and lists to the client.  All send() calls
# should be contained in this function.  Control signals from client_read could
# be passed to this thread through the associated Client object.  Make sure you
# use locks or similar synchronization tools to ensure that the two threads play
# nice with one another!
def client_write(client):
    while client.live_session:
        try:
            client.lock.acquire()
            if client.request == Protocol.LIST:
                print(SONGLIST)
                packet = pack_protocol(client.request.value, SONGLIST)
                client.conn.send(packet)
                client.request = None
            elif client.request == Protocol.PLAY:
                if client.current_stream is None:
                    packet = pack_protocol(Protocol.NULL.value, "Invalid request")
                    client.conn.send(packet)
                    client.reset()
                else:
                    packet = pack_protocol(client.request.value, "Playing: " + str(client.music_id))
                    client.conn.send(packet)
                    client.request = Protocol.DOWNLOAD
            
            elif client.request == Protocol.STOP:
                if client.music_id is None:
                    data = "No music to stop"
                else:
                    data = "Stopped"
                packet = pack_protocol(client.request.value, data)
                client.conn.send(packet)
                client.reset()

            elif client.request == Protocol.DOWNLOAD:
                if client.current_stream:
                    data = client.current_stream.read(READ_SIZE)
                    if len(data) == 0:
                        data = 'EOF'
                    packet = pack_protocol(client.request.value, data)
                    client.conn.send(packet)
                    client.request = None

            elif client.request == Protocol.QUIT:
                continue
            else:
                continue

        except socket.error:
            client.live_session = False
        except IOError:
            client.live_session = False
        finally:
            try:
                client.lock.release()
            except:
                pass


# TODO: Thread that receives commands from the client.  All recv() calls should
# be contained in this function.
def client_read(client):
    while client.live_session:
        try:
            data = client.conn.recv(SEND_BUFFER)
            if data:
                req, param = struct.unpack('ii', data)
                client.lock.acquire()

                if req == Protocol.LIST.value:
                    client.request = Protocol.LIST
                elif req == Protocol.PLAY.value:
                    client.reset()
                    client.request = Protocol.PLAY
                    if param in SONGS:
                        client.current_stream = open(SONGS[param], 'r', READ_SIZE)
                        client.music_id = param
                elif req == Protocol.STOP.value:
                    client.request = Protocol.STOP
                elif req == Protocol.DOWNLOAD.value:
                    client.request = Protocol.DOWNLOAD
                elif req == Protocol.QUIT.value:
                    client.reset()
                    client.request = Protocol.QUIT
                    client.live_session = False
                else:
                    continue

        except socket.error:
            client.live_session = False
        except IOError:
            client.live_session = False

        finally:
            try:
                client.lock.release()
            except:
                pass


def get_mp3s(musicdir):
    print("Reading music files...")
    songs_map = {}
    index = 0
    playlist = ["### Playlist ###"]

    for filename in os.listdir(musicdir):
        if filename.endswith(".mp3"):
            songs_map[index] = musicdir + "/" + filename
            playlist.append(str(index) + ": " + filename[:filename.rindex(".")])
            index += 1

    print("Found {0} song(s)!".format(len(songs_map)))
    
    return songs_map, "\n".join(playlist)


def start_server(port):
    """TODO: Listen on socket and print received message to sys.stdout"""
    HOST = None
    s = None

    for res in socket.getaddrinfo(HOST, port, socket.AF_UNSPEC, socket.SOCK_STREAM, 0, socket.AI_PASSIVE):
        af, socktype, proto, canonname, sa = res

        try:
            s = socket.socket(af, socktype, proto)
        except socket.error:
            s = None
            continue
        try:
            s.bind(sa)
            s.listen(QUEUE_LENGTH)
        except socket.error:
            s.close()
            s = None
            continue
        break

    if s is None:
        print('could not open socket from server')
        sys.exit(1)

    return s


def main():
    if len(sys.argv) != 3:
        sys.exit("Usage: python server.py [port] [musicdir]")
    if not os.path.isdir(sys.argv[2]):
        sys.exit("Directory '{0}' does not exist".format(sys.argv[2]))

    port = int(sys.argv[1])
    global SONGS, SONGLIST
    SONGS, SONGLIST = get_mp3s(sys.argv[2])

    threads = []

    s = start_server(port)

    # TODO: create a socket and accept incoming connections
    while True:
        try:
            conn, addr = s.accept()
            client = Client()
            client.connect(conn)
            t = Thread(target=client_read, args=(client,))
            threads.append(t)
            t.start()
            t = Thread(target=client_write, args=(client,))
            threads.append(t)
            t.start()
        except KeyboardInterrupt:
            print("Server closed by KeyboardInterrupt")
            break

    s.close()
    sys.exit(1)


if __name__ == "__main__":
    main()